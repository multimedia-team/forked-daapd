forked-daapd (26.4+dfsg1-2) unstable; urgency=medium

  * Team upload.

  [ Balint Reczey ]
  * Remove myself from Uploaders

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Sebastian Ramacher ]
  * debian/patches: Fix build with GCC 10 (Closes: #957217)

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 08 Aug 2020 08:58:07 +0200

forked-daapd (26.4+dfsg1-1) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure URI in Homepage field.

  [ Balint Reczey ]
  * debian/copyright: Convert to machine-readable format
  * debian/copyright: Add recent authors
  * debian/copyright: Exclude non-DFSG parts of the web interface
  * debian/watch: Add repacking options
  * New upstream version 26.4+dfsg1
  * Build depend on libwebsockets-dev and use it in build
  * Drop all patches, they are already present in upstream release
  * debian/gitlab-ci.yml: Add default CI config
  * Disable the web interface for now until replacing the non-DFSG parts
  * Skip building htdocs since the web interface is not shipped anyway and it
    fails to build
  * Add packaging TODO
  * Add basic autopkgtest
  * Ship empty /usr/share/forked-daapd/htdocs to let HTTPd thread start.
    This allows placing the web interface files there manually until they get
    packaged properly.
  * Drop runlevel override of dh_installinit

 -- Balint Reczey <rbalint@ubuntu.com>  Sat, 05 Jan 2019 01:10:26 +0700

forked-daapd (25.0-3) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Peter Green ]
  * Backport upstream patches for ffmpeg 4.0. (Closes: #888345)
  * Fix clean target.

  [ Sebastian Ramacher ]
  * debian/control: Bump Standards-Version.

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 28 Jul 2018 18:16:50 +0200

forked-daapd (25.0-2) unstable; urgency=medium

  * Remove upstart support
  * Restore ATV4/tvOS11 support using cherry-picked upstream fix
  * Bump compat level to 10, build depending on debhelper (>= 10)
  * Drop build-dependency on dh-systemd, autotools-dev and dh-autoreconf
    and drop now-default dh options

 -- Balint Reczey <rbalint@ubuntu.com>  Mon, 23 Oct 2017 00:46:13 +0200

forked-daapd (25.0-1) unstable; urgency=medium

  * New upstream version 25.0
    - Make the build reproducible (Closes: #859305)
    - Fix FTBFS with latest gperf (Closes: #869592)
    - Airplay device verification for Apple TV 4 w/tvOS 10.2 (uses libsodium)
  * Build-depend on libsodium-dev to enable Apple Device Verification support
  * Update d/rules hack to install .service file

 -- Balint Reczey <rbalint@ubuntu.com>  Mon, 31 Jul 2017 01:22:00 +0200

forked-daapd (24.2-2) unstable; urgency=medium

  * Update my Uploader email address to my Ubuntu one
  * Install systemd service enabled by default (Closes: #858696)

 -- Balint Reczey <rbalint@ubuntu.com>  Sun, 02 Apr 2017 12:06:56 +0200

forked-daapd (24.2-1) unstable; urgency=medium

  [ Espen Jürgensen ]
  * Depend of proper development package for inotify on kFreeBSD
    (Closes: #826785)

  [ Balint Reczey ]
  * Imported Upstream version 24.2
  * Enable PulseAudio support
  * Update d/control using cme fix
  * Depend on lsb-base instead of build-depending on it

 -- Balint Reczey <balint@balintreczey.hu>  Thu, 10 Nov 2016 13:31:39 +0100

forked-daapd (24.1-1) unstable; urgency=medium

  * New upstream release 24.1
    - support for Monkey's audio
    - fix build problems on some platforms (e.g. OpenWrt)
  * New upstream release 24.0
    - support for Chromecast audio
    - support more idv3 tags (eg. date released)
    - support more DAAP tags (eg. datereleased, hasbeenplayed)
    - fix problem with playlists not shown in correct order in Remote
    - autoselect devices based on priorities + keep devices selected
    - fix multiple FreeBSD filescanner bugs (like too many open files)
    - ALSA audio refurbished (prevent underrun/overrun, better AirPlay sync)
    - improved mpd command support (eg. lsinfo, move, queue autoplay start)
    - better mpd performance
    - timing changes to keep input and output in sync with player timer
    - prevent cache bloat (unscaled artwork or artwork for deleted tracks)
    - more intelligent Spotify artwork retrieval
    - artwork handling refactored
    - add generic output interface
    - add systemd service file
    - support for OSS4 dropped
    - support for old ffmpeg dropped
    - misc minor bugfixing
  * New upstream release 23.4
    - fix freeze problem on network stream disconnects
    - support for mp3 streaming
    - better ipv6 handling
    - option to hide singles from album/artist lists
    - misc MPD improvements, incl. new commands and zeroconf support
    - queue handling and transcoding refactored
    - libavresample/libswresample dependency changed to libavfilter
    - improved pairinghelper.sh script
  * Enable chromecasting
  * Stop shipping empty NEWS file
  * Bump policy version to 3.9.8
  * Ship service file for systemd

 -- Balint Reczey <balint@balintreczey.hu>  Mon, 16 May 2016 20:07:20 +0200

forked-daapd (23.3-1) unstable; urgency=medium

  * New upstream release 23.2
    - fix issue where volume gets set to -1 on startup of raop devices
    - plug various minor memleaks
    - audiobook improvements, eg resuming playback from saved position
    - live icy metadata
    - libevent 1 support removed
    - LastFM performance improvement
    - drop legacy ffmpeg stuff
    - drop legacy flac, musepack and wma scanner
  * Use canonical VCS field for git packaging repository
  * Stop adding ldconfig call to postinst and postrm
  * Enable all hardening options

 -- Balint Reczey <balint@balintreczey.hu>  Sat, 19 Dec 2015 10:41:32 +0100

forked-daapd (23.2-1) unstable; urgency=medium

  * New upstream release 23.2
    - Added MPD (Music Player Daemon) protocol support
    - Added support for smart playlists, live internet stream
      metadata/artwork (depending on versions of ffmpeg/libav/libevent),
      support for remote m3u's, and support for Remote's "Internet radio"
      category
    - Several stability fixes
  * Drop obsolete patch removing embedded AVL implementation
  * Switch back to libraries provided by Libav. They will be provided by
    ffmpeg when the archive-wide transition is finished.
  * Enable MPD support and also mention it in package description
  * Install README for smart playlists

 -- Balint Reczey <balint@balintreczey.hu>  Tue, 30 Jun 2015 17:45:35 -0500

forked-daapd (22.3+dfsg1-1) unstable; urgency=medium

  * New upstream release 22.3
  * Drop obsolete patches
  * Use system's libavl
  * Ship README.md instead of README
  * Update standards version to 3.9.6
  * Reformat build-dependencies
  * Switch to ffmpeg

 -- Balint Reczey <balint@balintreczey.hu>  Wed, 29 Apr 2015 16:45:43 +0200

forked-daapd (22.0-2) unstable; urgency=medium

  * Fix compatibility with iTunes 12.1 (Closes: #778995)
  * Fix two segfaults with upstream patches (Closes: #778996)
  * Fix playing audio locally (Closes: #779011)

 -- Balint Reczey <balint@balintreczey.hu>  Mon, 23 Feb 2015 11:54:01 +0100

forked-daapd (22.0-1) unstable; urgency=medium

  * Revert using pkg-config to determine FFmpeg linker flags
    See #758337for details.
  * Add watch file
  * Build-depend on libgcrypt20-dev instead of libgcrypt11-dev which
    became a transitional package
  * New upstream version 22.0
  * Enable last.fm scrobbling

 -- Balint Reczey <balint@balintreczey.hu>  Sat, 18 Oct 2014 00:39:52 +0200

forked-daapd (21.0-2) unstable; urgency=medium

  [ Espen Jürgensen ]
  * Update package description

  [ Andreas Cadhalpun ]
  * Use pkg-config to determine FFmpeg linker flags (Closes: #758337)

 -- Balint Reczey <balint@balintreczey.hu>  Sun, 17 Aug 2014 21:00:46 +0200

forked-daapd (21.0-1) unstable; urgency=low

  [ Balint Reczey ]
  * Switch to avahi-daemon in initscript LSB dependencies (Closes: #731616)
  * Update description in init script
  * Stop using flac and musepack libraries, use Libav codecs instead

  [ Espen Jürgensen ]
  * Update init script
  * Ship AUTHORS file

 -- Balint Reczey <balint@balintreczey.hu>  Sun, 15 Jun 2014 13:44:44 -0700

forked-daapd (20.0+git20140530+gc740e6e-1) unstable; urgency=low

  * Switch to new upstream using a development snapshot
  * Adopt package in the name of the Debian Multimedia Maintainers.
    Thanks to Julien Blache for maintaining the package for long years!
    (Closes: #688538)
  * Bump compat level to 9
  * Update homepage to new upstream
  * Add packaging VCS info
  * Use libevent instead of Grand Central Dispatch (Closes: #644839, #739503)
  * Drop patch for Libav 9 compatibility since upstream supports
    Libav 10 already (Closes: #739239)
  * Switch debian/rules to debhelper style
  * Build-depend on libavresample-dev
  * Source init functions in init script
  * Override false positive Lintian warnings about shlibs
  * Bump standards version without changes

 -- Balint Reczey <balint@balintreczey.hu>  Sat, 31 May 2014 23:17:37 +0700

forked-daapd (0.19gcd-3) unstable; urgency=low

  * QA upload, orphan package
  * Fix compatibility with libav9 (Closes: #720790)

 -- Moritz Muehlenhoff <jmm@debian.org>  Wed, 28 Aug 2013 19:11:37 +0200

forked-daapd (0.19gcd-2.1) unstable; urgency=low

  * Non-maintainer upload for release goal
  * Enable hardened build flags (Closes: #654147)

 -- Moritz Muehlenhoff <jmm@debian.org>  Fri, 11 May 2012 22:22:20 +0200

forked-daapd (0.19gcd-2) unstable; urgency=low

  * Move the GCD codebase to unstable.

 -- Julien BLACHE <jblache@debian.org>  Thu, 22 Sep 2011 19:15:11 +0200

forked-daapd (0.19gcd-1) experimental; urgency=low

  * New upstream release, GCD codebase.

  * debian/patches/dacp-unknown-prop-loop-fix.patch:
    + Removed; merged upstream.

 -- Julien BLACHE <jblache@debian.org>  Sun, 11 Sep 2011 16:19:40 +0200

forked-daapd (0.19-1) unstable; urgency=low

  * New upstream release.

  * debian/patches/dacp-unknown-prop-loop-fix.patch:
    + Removed; merged upstream.

 -- Julien BLACHE <jblache@debian.org>  Sun, 11 Sep 2011 15:51:59 +0200

forked-daapd (0.18gcd-2) experimental; urgency=low

  * debian/patches/dacp-unknown-prop-loop-fix.patch:
    + Added; fix infinite loop on unknown DACP property (closes: #637864).

 -- Julien BLACHE <jblache@debian.org>  Mon, 15 Aug 2011 13:05:51 +0200

forked-daapd (0.18gcd-1) experimental; urgency=low

  * New upstream release; GCD codebase.

 -- Julien BLACHE <jblache@debian.org>  Sun, 07 Aug 2011 12:16:39 +0200

forked-daapd (0.18-1) unstable; urgency=low

  * New upstream release.

  * debian/forked-daapd.logrotate:
    + Add create option.

 -- Julien BLACHE <jblache@debian.org>  Sun, 07 Aug 2011 11:40:27 +0200

forked-daapd (0.17gcd-1) experimental; urgency=low

  * New libdispatch-based codebase.

  * debian/control:
    + Adjust build-dependencies: remove libevent-dev, add libdispatch-dev,
      libblocksruntime-dev, libtre-dev, clang.
  * debian/copyright:
    + Adjust for new codebase.

 -- Julien BLACHE <jblache@debian.org>  Mon, 20 Jun 2011 18:15:23 +0200

forked-daapd (0.17-1) unstable; urgency=low

  * New upstream release.
    + Adds support for libav 0.7.

 -- Julien BLACHE <jblache@debian.org>  Mon, 20 Jun 2011 17:59:55 +0200

forked-daapd (0.16-2) unstable; urgency=low

  * debian/control:
    + Add missing dependency on psmisc, as we're using killall in the logrotate
      script (closes: #628888).

 -- Julien BLACHE <jblache@debian.org>  Thu, 02 Jun 2011 15:51:50 +0200

forked-daapd (0.16-1) unstable; urgency=low

  * New upstream release.

 -- Julien BLACHE <jblache@debian.org>  Sat, 30 Apr 2011 11:48:15 +0200

forked-daapd (0.15-1) unstable; urgency=low

  * New upstream release.

  * debian/control:
    + Add build-dependency on gperf.
    + Bump Standards-Version to 3.9.2 (no changes).
  * debian/copyright:
    + Update list of copyright holders.

 -- Julien BLACHE <jblache@debian.org>  Sat, 09 Apr 2011 11:41:55 +0200

forked-daapd (0.14-1) unstable; urgency=low

  * New upstream release.

  * debian/dirs:
    + Add /var/cache/forked-daapd/backups.
  * debian/forked-daapd.postinst:
    + Backup the database before upgrade when needed.
  * debian/forked-daapd.README.Debian:
    + Document database backup upon upgrade.

 -- Julien BLACHE <jblache@debian.org>  Fri, 25 Mar 2011 18:43:44 +0100

forked-daapd (0.13-1) unstable; urgency=low

  * New upstream release.
    + Improved mDNS address resolution (closes: #608448).
    + Trim metadata strings on insert/update (closes: #596014).
    + Added support for sort fields (closes: #596823).

  * debian/forked-daapd.init:
    + Fix restart action (closes: #616112).

  * debian/patches:
    + Drop all patches.

 -- Julien BLACHE <jblache@debian.org>  Sat, 12 Mar 2011 14:19:09 +0100

forked-daapd (0.12~git0.11-125-gca72ee5-4) unstable; urgency=low

  * debian/control:
    + Add build-dependency on libgpg-error-dev.

  * debian/patches/fix_final_link.patch:
    + Added; fix missing -lrt -lgpg-error (closes: #614727).

 -- Julien BLACHE <jblache@debian.org>  Wed, 23 Feb 2011 17:28:30 +0100

forked-daapd (0.12~git0.11-125-gca72ee5-3) unstable; urgency=low

  * debian/patches/murmurhash_32bit_fix.patch:
    + Added; fix improper handling of data buffer tail.
  * debian/patches/no_scoped_address_in_sdp.patch:
    + Added; don't let out scoped addresses in SDP payload.

 -- Julien BLACHE <jblache@debian.org>  Mon, 01 Nov 2010 15:36:39 +0100

forked-daapd (0.12~git0.11-125-gca72ee5-2) unstable; urgency=low

  * debian/patches/doc_update.patch:
    + Added; small documentation updates.
  * debian/patches/db_crasher_fix.patch:
    + Added; fix a crasher in the database code.
  * debian/patches/code_typo_fix.patch:
    + Added; fix a typo, checking the wrong variable.
  * debian/patches/compilation_handling_fix.patch
    + Added; fix handling of album_artist for compilations.
  * debian/patches/gcrypt_init_eh_fix.patch:
    + Added; set the proper exit value in the gcrypt EH path.
  * debian/patches/plug_full_uri_leak_in_eh.patch:
    + Added; plug a memory leak in EH.
  * debian/patches/remove_dup_mstm_tag.patch:
    + Added; remove a duplicate mstm tag in DAAP server-info.
  * debian/patches/seek_computations_fix.patch:
    + Added; fix target & position computations for seeking.

 -- Julien BLACHE <jblache@debian.org>  Mon, 20 Sep 2010 18:58:44 +0200

forked-daapd (0.12~git0.11-125-gca72ee5-1) unstable; urgency=low

  * Git snapshot 0.11-125-gca72ee5.

  * debian/forked-daapd.postrm:
    + Prevent rmdir failure when /var/cache/forked-daapd doesn't exist
      (closes: #591868).

 -- Julien BLACHE <jblache@debian.org>  Fri, 06 Aug 2010 17:15:18 +0200

forked-daapd (0.12~git0.11-117-gd9f5e2a-2) unstable; urgency=low

  * Re-apply changes from 0.12~git0.11-92-ge396906-2 that got lost by
    erroneously preparing the previous upload using the packaging from
    0.12~git0.11-92-ge396906-1. Doh!

 -- Julien BLACHE <jblache@debian.org>  Mon, 02 Aug 2010 18:21:05 +0200

forked-daapd (0.12~git0.11-117-gd9f5e2a-1) unstable; urgency=low

  * Git snapshot 0.11-117-gd9f5e2a.

  * debian/control:
    + Bump Standards-Version to 3.9.1 (no changes).

 -- Julien BLACHE <jblache@debian.org>  Sun, 01 Aug 2010 15:13:03 +0200

forked-daapd (0.12~git0.11-92-ge396906-2) unstable; urgency=low

  * debian/control:
    + Build-dep on libasound2-dev on Linux only.
    + Add a build-dep on oss4-dev on kFreeBSD.
    + Build-dep on libunistring-dev (>= 9.0.3) as previous versions ship broken
      headers.
  * debian/rules:
    + Build with OSS4 on kFreeBSD.

 -- Julien BLACHE <jblache@debian.org>  Thu, 22 Jul 2010 17:54:11 +0200

forked-daapd (0.12~git0.11-92-ge396906-1) unstable; urgency=low

  * Git snapshot 0.11-92-ge396906.

  * Moved to unstable.

 -- Julien BLACHE <jblache@debian.org>  Thu, 22 Jul 2010 09:45:05 +0200

forked-daapd (0.12~git0.11-80-g65d3651-1) experimental; urgency=low

  * Git snapshot 0.11-80-g65d3651.

  * Initial upload (closes: #587958).
    + Upload to experimental until antlr3 can move to testing.

  * Moved to source version 3.0 (quilt).

  * debian/control:
    + Add Build-dep on libunistring-dev.
    + Bump antlr3 build-dep to (>= 3.2-3).
    + Bump libantlr3c-dev build-dep to (>= 3.2).
    + Bump Standards-Version to 3.9.0 (no changes).
    + Rework descriptions.
  * debian/copyright:
    + Updated.

 -- Julien BLACHE <jblache@debian.org>  Sat, 03 Jul 2010 11:26:12 +0200

forked-daapd (0.12~0.11-71-g7d858d6-2) unstable; urgency=low

  * debian/control:
    + Add missing build-dep: libswscale-dev, libavutil-dev.

 -- Julien BLACHE <jblache@debian.org>  Mon, 17 May 2010 17:53:15 +0200

forked-daapd (0.12~0.11-71-g7d858d6-1) unstable; urgency=low

  * Git snapshot 0.11-71-g7d858d6.

  * debian/control:
    + Added build-deps: libasound2-dev, zlib1g-dev.
    + Bumped libsqlite3-dev build-dep to (>= 3.6.23.1-2) for update notify
      API support.

 -- Julien BLACHE <jblache@debian.org>  Sat, 15 May 2010 16:53:17 +0200

forked-daapd (0.11~0.10-201-g2dde2f1-1) unstable; urgency=low

  * Git snapshot 0.10-201-g2dde2f1.

  * debian/control:
    + Add build-dep on libgcrypt11-dev.

 -- Julien BLACHE <jblache@debian.org>  Sun, 14 Feb 2010 10:14:21 +0100

forked-daapd (0.11~0.10-34-g8ec1fa3-1) unstable; urgency=low

  * Git snapshot 0.10-34-g8ec1fa3.

  * debian/control:
    + Add build-dep on libplist-dev (>= 0.16).
  * debian/rules:
    + Enable support for iTunes Music Library (XML playlist).

 -- Julien BLACHE <jblache@debian.org>  Sun, 13 Dec 2009 10:04:04 +0100

forked-daapd (0.10-1) unstable; urgency=low

  * Initial release.

 -- Julien BLACHE <jblache@debian.org>  Sun, 20 Sep 2009 15:42:18 +0200
